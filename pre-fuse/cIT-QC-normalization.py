#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Program to perform cIT-QC normalization in .vidjil files.

    Developed at the Boldrini Research Center, Brazil, in 2023.
    
    Contributors: 
      Antonio Vítor Ribeiro
      Guilherme Navarro Nilo Giusti
      João Meidanis
      Florian Thonier

'''

############################################################
### imports

import json
import sys
import argparse
import datetime
from collections import defaultdict 

############################################################
### routines

def getPrevalent(reads_by_germlines):
    """ gets prevalent germline from sample
    If prevalent is IGK+ and IGK > 10% of IGK+, return IGK

    Args:
        reads_by_germlines (dict): Dict of reads by germline (key:int)

    Returns:
        str: Locus of prevalent germline
    """
    prevalent = max(reads_by_germlines, key=lambda k: max(reads_by_germlines[k]))
    
    # Specific case of IGK+
    if prevalent == "IGK+" and "IGK" in reads_by_germlines.keys() and reads_by_germlines["IGK"] != 0:
        if float(reads_by_germlines["IGK"][0])/float(reads_by_germlines["IGK+"][0]) > 0.1:
            prevalent = "IGK"
    return  prevalent


def getLabelizedClones(clones, prevalent):
    """ Returns list of labelised clones in sample

    Args:
        clones (list): List of clones
        prevalent (str): prevalent germline/locus

    Returns:
        list: List of labelised clones in sample
    """
    label_clones = [clone for clone in clones if 'label' in clone and prevalent in clone["germline"]]
    return label_clones

# 
def get_c_reads_cells(labelized_clones, copy_num):
    """ Calculates total control cells and reads

    Args:
        labelized_clones (list): List of spikes clones in sample
        copy_num (int): Number of expected copy for each spike

    Returns:
        int, int: Total number of reads and cell
    """
    total_spikes = [spike["reads"][0] for spike in labelized_clones]
    unique_spike = [spike["label"] for spike in labelized_clones]
    # for spike in labelized_clones:
    #     total_spikes.extend(spike['reads'])
    #     unique_spike.append(spike['label'])


    total_c_reads = sum(total_spikes)
    total_c_cells = len(set(unique_spike)) * copy_num

    print(f"{total_c_reads=}")
    print(f"{total_c_cells=}")
    
    return total_c_reads, total_c_cells

# 
def calc_drm(data, total_c_reads, total_c_cells, total_cells, segmented_reads):
    """ Calculates MRD value

    Args:
        data (dict): Data imported from vidjil file
        total_c_reads (int): Total number of reads of spikes in samples
        total_c_cells (int): Total number of expected cells of spikes in sample
        total_cells (int): Total number of cells present in sample (expected from wet lab manipulation, set in spike config files)
        segmented_reads (int): Segmented reads from vidjil data

    Returns:
        vidjil data, factor: _description_
    """
    factor = float(total_c_cells) / float(total_c_reads)
    factor_cells = factor / float(total_cells)
    print( f"calc_drm:\n\t{factor=};\n\t{factor_cells=}")
    for clone in data['clones']:
        clone['normalized_reads'] = [clone['reads'][0] * float(factor_cells) * segmented_reads]
    return data, factor

# 
def verification(labelized_clones, prevalent):
    """ Checks for missing cIT-QC

    Args:
        labelized_clones (list): List of present spikes in data
        prevalent (str): Locus of prevalent germline

    Returns:
        str: Message of warning if missing spike
    """
    con     = [spike["label"] for spike in labelized_clones]
    missing = [spike for spike in spike_dict[prevalent] if spike not in con]
    
    if len(missing) > 0:
        return "cIT-QC not found: %sx\n\t%s" % (len(missing), "\n\t".join(missing))
    else:
        return "All cIT-QC have been found" 


def computeReadsOfSpikes(data):
    """Include in reads field some values on spikes present in this sample

    Args:
        data (vidjil): Vidjil data content
    """
    data["reads"]["spikes"] = {"reads": defaultdict(lambda: [0]), "clones": defaultdict(lambda: [0])}
    for clone in data["clones"]:
        if 'label' in clone:
            data["reads"]["spikes"]["reads"][clone["germline"]][0] += clone["reads"][0]
            data["reads"]["spikes"]["clones"][clone["germline"]][0] += 1
            data["reads"]["spikes"]["reads"]["total"][0]  += clone["reads"][0]
            data["reads"]["spikes"]["clones"]["total"][0] += 1
    return


############################################################
### main program

if __name__ == '__main__':

    log  = "MRD pipeline post-processus\n"
    log += f"(cIT-QC normalization; {datetime.datetime.now().strftime('%Y-%m-%d %H:%M')} # {' '.join(sys.argv)}\n"
    # print(log)


    DESCRIPTION = 'Script to include cIT-QC normalization on a vidjil result file (As concepted by Euroclonality)\nImplemented by Boldrini team (Antonio Vítor Ribeiro, Guilherme Navarro Nilo Giusti, João Meidanis)'
    
    parser = argparse.ArgumentParser(description= DESCRIPTION,
                                     epilog='''Example: python %(prog)s -- input filein.vidjil --output fileout.vidjil''',
                                     formatter_class=argparse.RawTextHelpFormatter)

    group_options = parser.add_argument_group()
    group_options.add_argument('-i', '--input', help='Vidjil input file', required=True)
    group_options.add_argument('-o', '--output', help='Vidjil output file with cIT-QC normalization', required=True)
    group_options.add_argument('--index', default=0, type=int, help='Position of vidjil file in list of files given to fuse.py')
    group_options.add_argument('-c', '--config', help='Spikes configuration to use', default=False)
    group_options.add_argument('--verbose', action='store_true', help='run script in verbose mode')
    group_options.add_argument('--indent',  action='store_true', help='Use indent output (take more space; use only for dev)')


    args = parser.parse_args()
    inf  = args.input
    outf = args.output
    msgs = args.verbose
    conf_file = args.config
    indent = 2 if args.indent else False
    sample_pos = int(args.index)
    print ( "verbose: %s" % msgs)

    # Read vidjil input analysis file  
    pre = False
    if "," in inf:
        # input with preprocess file
        pre = inf.split(",")[1] 
        inf = inf.split(",")[0] 

    with open(inf) as inp:
        data = json.load(inp)

    if pre:
        with open(pre) as fi_pre:
            data_preprocess = json.load(fi_pre)["pre_process"]
    else:
        data_preprocess = {}
    print( f"{data_preprocess=}" )

    try:
        # Import spikes data from label file use in vidjil file
        # Need to be on the same server
        conf_file = data["config"]["label-json"] if not conf_file else conf_file
        with open(conf_file) as conf:
            config = json.load(conf)["config"]
            log += f"Loaded conf: {config['name']}\n"

            spike_dict = defaultdict(list)
            for locus in config["germ_dict"]:
                spike_dict[locus] = [spike["name"] for spike in config["labels"] if spike["locus"] == locus]


        # process data
        segmented_reads = data['reads']['segmented'][0]
        prevalent = getPrevalent(data['reads']['germline'])
        log += f"Prevalent germline: {prevalent}\n" 
        if prevalent == False:
            log += "Warnings: No clonotypes found in this vidjil file. No MRD computable\n"

        labelized_clones = getLabelizedClones(data['clones'], prevalent)
        computeReadsOfSpikes(data)
        
        warnings = verification(labelized_clones, prevalent)
        log += f"Warnings: {warnings}"



        total_c_reads, total_c_cells = get_c_reads_cells(labelized_clones, config["copy_num"])
        log += f"cIT-QC total reads = {total_c_reads}\n"
        log += f"Reads: {total_c_reads}/ {data['reads']['germline'][prevalent][0]} reads\n"

        if not total_c_reads:
            log += f"No spikes present, no MRd computable.\n"
            updated_data = data
        else:
            # Compute MRD
            sample_type = 'diag' if sample_pos == 0 else 'follow-up'
            total_cells = config["total_cells"][sample_type]
            print( f"Total cells set to {total_cells} as index of sample index is {sample_pos} (type {type(sample_pos)}) ({sample_type})")
            updated_data, factor = calc_drm(data, total_c_reads, total_c_cells, total_cells, segmented_reads)
            log += f"Quantification factor: {factor}\n"
        
    
        ## Write log in file
        if not "pre-fuse" in updated_data["samples"]:
            updated_data["samples"]["pre-fuse"] = {}
        updated_data["samples"]["pre-fuse"] = {"log": [log]}


        # write output file
        with open(outf, 'w') as of:
            print(json.dumps(updated_data, sort_keys=True, indent=indent), file=of)

    except Exception as e:
        print(e)
    finally:
        print(log)