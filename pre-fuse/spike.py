#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' Program to perform spike-in normalization in .vidjil files.

    Developed at the Boldrini Center, Brazil, in 2018-2020.
'''

############################################################
### imports

from __future__ import print_function
import sys
import json
import os
import math
import argparse

############################################################
### constants

version = 'S0.08'
UNI = 'UNI'                     # except Vidjil leaves cluster to user
### maximum reads for a given spike-in allowed in diagnostic samples
DIAGMAX = 50.0

############################################################
### routines

############################################################
def linearRegression(y, x):
    '''
    function for curve fitting y = ax
    just slope, no intercept, to force (0,0)
    '''
    lr = {}
    n = len(y)
    assert(n == len(x))
    lr['n'] = n
    sum_x = 0.0
    sum_y = 0.0
    sum_xy = 0.0
    sum_xx = 0.0
    sum_yy = 0.0

    for i in range(n):

        sum_x += x[i]
        sum_y += y[i]
        sum_xy += (x[i]*y[i])
        sum_xx += (x[i]*x[i])
        sum_yy += (y[i]*y[i])

    lr['slope'] = sum_xy / sum_xx
    denom = (n*sum_xx-sum_x*sum_x)*(n*sum_yy-sum_y*sum_y)
    lr['r2'] = math.pow((n*sum_xy - sum_x*sum_y),2)/denom if denom > 0 else 'N/A'
    lr['s'] = math.sqrt((sum_yy - sum_xy*sum_xy/sum_xx)/n)
    
    return lr

############################################################

def prevalentGermline(germlines):
    reads = {}
    prevalent = ""
    for g in germlines:
        germline = g[0:3] # fiable
        if germline in reads:
            reads[germline] += germlines[g][0]
        else:
            reads[germline] = germlines[g][0]
        if prevalent == "" or reads[prevalent] < reads[germline]:
            prevalent = germline
    return prevalent, reads[prevalent]

############################################################
### Construct spike table from clones
### Note: just clones from prevalent family

def spikeTable(data, prevalent):
    reads = {}
    ## get config
    spikes = data['config']['labels']
    
    ## find spikes in data, stamp name, and augment table
    for clone in data['clones']:
        c1 = 'label' in clone
        c2 = 'germline' in clone and clone['germline'][0:3] == prevalent
        if c1 and c2:
            ## spike-in clone
            label = clone['label']
            if 'name' in clone:
                ## change clone name
                clone['name'] = label + ' ' + clone['name']
            else:
                ## add spike name to clone anyway
                clone['name'] = label
            ## grab read data
            a = clone['reads']
            if len(a) > 1:
                print('  *** reads array with many elements', file=sys.stderr)
            ## add reads
            if label not in reads:
                reads[label] = 0.0
            reads[label] += a[0]
    for spike in spikes:
        ## 'label' in Vidjil file is 'name' is spike JSON
        spike['reads'] = reads[spike['name']] if spike['name'] in reads else 0
    if msgs:
        lastFam = ''
        for spike in sorted(spikes, key=lambda spk: spk['family']+" {0:03}".format(int(spk['copies']))):
            if spike['family'] != lastFam:
                print("=" * (30 + 1 + 7 + 1 + 4), file=sys.stderr)
            lastFam = spike['family']
            fmtStr = '{0:30} {1:7} {2:>4}'
            print(fmtStr.format(spike['name'], spike['reads'], spike['copies']), file=sys.stderr)
        print("=" * (30 + 1 + 7 + 1 + 4), file=sys.stderr)
    return spikes

############################################################

def computeCoefficients(spikes):
    '''
    curve-fitting
    computes f for each user family
    plus UNI coefficient
    if family not dense enough or Pearson < 0.8 use UNI
    '''

    coeff = {}
    r2 = {}
    perr = {}
    good = {}                   # family good for fitting or not
    
    ### UNI coefficient (only if enough spikes)
    ### uses one point per copy number
    ### test for not enough items

    copyNos = list(set([int(spike['copies']) for spike in spikes]))
    ## data for linear model
    abundCopyNos = []
    avgReads = []
    ## UNI records for labels data structure
    unilist = []
    if msgs:
        print('copyNos: ', copyNos)
    for copyNo in copyNos:
        reads = [ spike['reads'] for spike in spikes
                  if int(spike['copies']) == copyNo
                  and spike['reads'] > 0
        ]
        if len(reads) > 0:
            abundCopyNos.append(copyNo)
            avgReads.append(sum(reads)/len(reads))
    if msgs:
        print('abCopyNs: ', abundCopyNos)
        print('avgReads: ', avgReads)
    fewAbundCopyNos = len([a for a in avgReads if a > DIAGMAX]) <= 1
    if fewAbundCopyNos or max(avgReads) <= DIAGMAX:
        ## not enough spikes; probably diagnostic sample
        print('** Not enough spike-ins **', file=sys.stderr)
        print('** No normalization performed **', file=sys.stderr)
    else:
        ## plenty of spikes; compute universal coefficient
        if msgs >= 2:
            fmtStr = 'UNI regression: {0} {1}'
            print(fmtStr.format(abundCopyNos, avgReads), file=sys.stderr)
        lr = linearRegression(abundCopyNos, avgReads)
        coeff[UNI] = lr['slope']
        r2[UNI] = lr['r2']
        perr[UNI] = lr['s']
        good[UNI] = True
        ## UNI records for labels data structure
        for i in range(len(abundCopyNos)):
            strCopyNo = str(abundCopyNos[i])
            unilist.append({
                'sequence': None,
                'copies': strCopyNo,
                'reads': str(round(avgReads[i])),
                'family': UNI,
                'name': UNI + '-' + strCopyNo})
        if msgs:
            fmtStr = 'Uni coefficient estimation: {0:15.13f} s: {1:5.1f} r2: {2:15.13f}'
            print(fmtStr.format(coeff[UNI], perr[UNI], r2[UNI]), file=sys.stderr)

        ### family coefficients
        ### also use one point per copy number
        ### test for not enough points and r2 <= 0.8

        fams = list(set([spike['family'] for spike in spikes]))
        if msgs:
            print('Fams: {0}'.format(fams))
        for fam in fams:
            copyNos = list(set([int(spike['copies']) for spike in spikes if spike['family'] == fam]))
            avgReads = []
            for copyNo in copyNos:
                reads = [ spike['reads'] for spike in spikes
                          if spike['family'] == fam and
                          int(spike['copies']) == copyNo]
                ## len(reads) is never zero because spike['copies'] is in copyNos
                avgReads.append(sum(reads)/len(reads))
            if len(avgReads) <= 1 or max(avgReads) <= DIAGMAX:
                ## not enough points; family not good
                good[fam] = False
            else:
                ### fit curve
                if msgs >= 2:
                    fmtStr = '{0} regression: {1} {2}'
                    print(fmtStr.format(fam, copyNos, avgReads), file=sys.stderr)
                lr = linearRegression(copyNos, avgReads)
                coeff[fam] = lr['slope']
                r2[fam] = lr['r2']
                perr[fam] = lr['s']
                good[fam] = lr['r2'] >= 0.8
                if msgs:
                    fmtStr = '{0} coefficient estimation: {1:15.13f} s: {2:5.1f} r2: {3:15.13f}'
                    print(fmtStr.format(fam, coeff[fam], perr[fam], r2[fam]), file=sys.stderr)

    return coeff, r2, good, unilist

############################################################
### add information neeeded to compute MRD
###
### data: entire .vidjil JSON structure
### coeff: dict with normalization coefficients for each family
### r2: Pearson's r2 for each family
### ampl_coeff: amplification coefficient (total prevalent/total spikes)

def addMRDinfo(data, coeff, r2, good, ampl_coeff, prevalent):
    if msgs:
        print('Addding MRD information and printing output file', file=sys.stderr)

    data['mrd'] = {}
    data['mrd']['coefficients'] = [ coeff ]
    data['mrd']['prevalent']  = [ prevalent ]
    data['mrd']['R2']     = [ r2 ]
    data['mrd']['good']     = [ good ]
    data['mrd']['labels'] = [ data['config']['labels'] ]
    if not coeff:
        return
    
    data['mrd']['ampl_coeff'] = [ ampl_coeff ]

############################################################
### command line, initial msg

if __name__ == '__main__':

    print("#", ' '.join(sys.argv))

    DESCRIPTION = 'Script to include spike-nomalization on a vidjil result file'
    
    #### Argument parser (argparse)

    parser = argparse.ArgumentParser(description= DESCRIPTION,
                                    epilog='''Example:
  python %(prog)s --input filein.vidjil --ouput fileout.vidjil''',
                                    formatter_class=argparse.RawTextHelpFormatter)


    group_options = parser.add_argument_group() # title='Options and parameters')
    group_options.add_argument('-i', '--input',  help='Vidjil input file')
    group_options.add_argument('-o', '--output', help='Vidjil output file with normalization')
    group_options.add_argument('--verbose', action='store_true', default=False, help='run script in verbose mode')
    
    args = parser.parse_args()

    inf  = args.input
    outf = args.output
    msgs = args.verbose
    print ( "verbose: %s" % msgs)

    # read input file
    if msgs:
        print('Reading input file', file=sys.stderr)

    with open(inf) as inp:
        data = json.load(inp)

    # process data
    ## find prevalent germine and sum of its reads
    prevalent, spg = prevalentGermline(data['reads']['germline'])
    ## spike table with prevalent germline spikes only
    spikes = spikeTable(data, prevalent)
    coeff, r2, good, unilist = computeCoefficients(spikes)
    spk = sum([spike['reads'] for spike in spikes])
    ampl_coeff = spg/spk if spk > 0 else 'N/A'
    ## add UNI records to labels list
    data['config']['labels'].extend(unilist)
    addMRDinfo(data, coeff, r2, good, ampl_coeff, prevalent)

    # write output file
    with open(outf, 'w') as of:
        print(json.dumps(data, sort_keys=True, indent=2), file=of)
