Program to perform cIT-QC normalization in .vidjil files.

Developed at the Boldrini Research Center, Brazil, in 2023.

Contributors: 
  Antonio Vítor Ribeiro
  Guilherme Navarro Nilo Giusti
  João Meidanis

This script allow to make a normalization based on MRD configuraton given and spikin reads found in a sequencing file.

To work, this script should be sibling of a configuration file that will be call by vidjil algo to label specific clonotype, and by fuse preprocess to make normalisation. 
A prefill configuration file is given with EuroMRD data (file `cIT-QC-config.json`). This include spikes definition, sequences, number of copy and cells expected.

An example of configuration that use this script:

* Vidjil command: `-c clones -z 100 -r 1 -g germline/homo-sapiens.g -e 1 -2 -d -w 50 --label-json  /usr/share/vidjil/tools/script/pre-fuse/cIT-QC-config.json`
* Fuse command: `-t 100 -d lenSeqAverage -t 100  --pre "cIT-QC-normalization.py --verbose"`


Note that for vidjil use by docker image; path should be `/usr/share/vidjil/tools/script/pre-fuse/`.