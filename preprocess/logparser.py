#!/usr/bin/python

import argparse
import sys
import json
from datetime import datetime

class LogParser:
    """
    Main class of type LogParser.
    Call 'export' function to make extraction of content of raw log file into structured data. 
    By default, will take every lines in "key: value" string format to return {key: value}
    Store data into a json file that will be used later at fuse step of analysis.
    """
    def __init__(self, log_file):
        if isinstance(log_file, str):
            log_file   = open(log_file, "r")
        self.log_file = log_file

    def export(self, preprocessArgs, output_file, one_line=True):
        parsed_log = self.parse() # Call parse function. This function can be redefined in child class.
        parsed_log['pre_process']['commandline'] = [preprocessArgs]
        parsed_log['number'] = 1
        #parsed_log['pre_process']['original_names'] = [path_file]
        timestamp = [datetime.now().strftime('%Y-%m-%d %H:%M:%S')]
        parsed_log['pre_process']['run_timestamp'] = timestamp
        parsed_log['vidjil_json_version'] = 'TODO'
        parsed_log['reads'] = {}
        # default for merging script
        if "stats" in parsed_log['pre_process']:
            if "combined_pairs" in parsed_log['pre_process']['stats']:
                parsed_log['reads']['merged'] = parsed_log['pre_process']['stats']['combined_pairs']
            if "total_pairs" in parsed_log['pre_process']['stats']:
                parsed_log['reads']['total'] = parsed_log['pre_process']['stats']['total_pairs']

            if "reads_segmented" in parsed_log['pre_process']['stats']:
                parsed_log['reads']['merged'] = parsed_log['pre_process']['stats']['reads_segmented']
            if "reads_initial" in parsed_log['pre_process']['stats']:
                parsed_log['reads']['total'] = parsed_log['pre_process']['stats']['reads_initial']

        with open(output_file, 'w') as vidjil_file:
            if one_line:
                vidjil_file.write(json.dumps(parsed_log))
            else:
                vidjil_file.write(json.dumps(parsed_log, sort_keys=True, indent=2))

    def parse(self):
        """
        main function to transform content of log into structured data
        Can be redefined in child classes
        """
        parsed_log = {}
        parsed_log['stats'] = {}
        parsed_log['parameters'] = {}
        log_line = self.readline(strip=True) # strip to remove trailing spaces
        while log_line:
            if log_line != "" and ":" in log_line:
                key, value = self.get_key_value(log_line)
                # print( f"KEY: {key}; VALUE {value}; LOGLINE: {log_line};")
                parsed_log["stats"][key] = [value]
            log_line = self.readline(True)

        result = {'pre_process': parsed_log}
        return result

    def readline(self, strip=False):
        """ Return next line of the log file; False if end line reach """
        log_line = self.log_file.readline()
        if(strip):
            return log_line.strip()
        return log_line

    def convert(self, value):
        """
        Try to convert value data into various format
        continue while no correct format found, return string format if no other available
        If don't succeed, also split and retry conversion on first element; ex: "4 reads"
        int => float => (recursive call on split)
        Can be redefined in child classes
        """
        try:
            tmp = int(value)
        except ValueError:
            try:
                tmp = float(value)
            except ValueError:
                try:
                    tmp = value.split()[0]
                    if tmp != value:
                        return self.convert(tmp)
                    tmp = value
                except Exception:
                    tmp = value
        return tmp

    def get_key_value(self, line):
        """ 
        split a line on ':' character
        return first element as key and try an automatic conversion of the second
        """
        split_line = line.split(':')
        key = split_line[0].lower().strip().replace(' ', '_').replace('"', '').replace('__', '_')
        value = split_line[1].strip()
        value = self.convert(value)
        return (key,value)


class FlashLogParser(LogParser):
    """
    FlashLogParser; new class to convert specific log of Flash2 tools
    Replace default function 'parse' and 'readline'
    """
    def parse(self):
        parsed_log = {}
        log_line = self.readline()
        while log_line:
            log_line = log_line.strip()
            if(log_line.startswith('WARNING')):
                if "warning" not in parsed_log.keys():
                    parsed_log['warnings'] = [[]]
                key, value = self.get_key_value(log_line)
                parsed_log['warnings'][0].append(value)

                pass

            if(log_line.startswith('Starting FLASH')):
                parsed_log['producer'] = [' '.join(log_line.split()[1:])]

            if(log_line in ['Input files:', 'Output files:']):
                key = log_line.lower().split()[0]
                parsed_log[key] = []
                arr = []
                log_line = self.readline(True)
                while(log_line != ""):
                    arr.append(log_line)
                    log_line = self.readline(True)
                parsed_log[key].append(arr)

            if(log_line == 'Read combination statistics:'):
                parsed_log['stats'] = {}
                log_line = self.readline(True)
                while(log_line != ""):
                    key, value = self.get_key_value(log_line)
                    parsed_log['stats'][key] = [value]
                    log_line = self.readline(True)

            if(log_line == 'Parameters:'):
                parsed_log['parameters'] = {}
                log_line = self.readline(True)
                while(log_line != ""):
                    key, value = self.get_key_value(log_line)
                    parsed_log['parameters'][key] = [value]
                    log_line = self.readline(True)


            if("time taken" in log_line):
                key, value = self.get_key_value(log_line)
                parsed_log['stats'][key] = [value]

            log_line = self.readline()
        result = {'pre_process': parsed_log}
        return result

    def readline(self, strip=False):
        log_line = self.log_file.readline()
        log_line = log_line.replace('[FLASH]', '')
        if(strip):
            return log_line.strip()
        return log_line


class VidjilLogParser(LogParser):
    """
    VidjilLogParser; new class to convert specific log of Vidjil
    Replace default function 'parse' and 'readline'
    """
    def parse(self):
        parsed_log = {"stats": {}}
        log_line = self.readline()
        while log_line:
            log_line = log_line.strip()
            if(log_line.startswith('# version:')):
                parsed_log['stats']["vidjil-version"] = [log_line.replace("# version: ", "")]
            if(log_line.startswith('Germlines loaded:')):
                parsed_log['stats']["germline"] = [log_line.replace("Germlines loaded: ", "")]

            if(log_line.startswith('==> ') and log_line.endswith(" sequences")):
                if " approx." in log_line:
                    log_line = log_line.replace(" approx.", "") # new algo output
                value = log_line.replace("==> ", "").replace(" sequences", "")
                parsed_log['stats']["reads_initial"] = [int(value)]

            if(log_line.startswith('==> junction')):
                value = log_line.split("in")[1].split(" reads")[0]
                parsed_log['stats']["reads_segmented"] = [int(value)]

            if(log_line.startswith('==> found')):
                value = log_line.split(" windows")[0].split('==> found')[1]
                parsed_log['stats']["junction"] = [int(value)]

                arr = ""
                log_line = self.readline(False)
                while(log_line != "\n"):
                    arr += log_line
                    log_line = self.readline(False)
                parsed_log["stats"]["vidjil-log"] = [arr]


            if("time taken" in log_line):
                key, value = self.get_key_value(log_line)
                parsed_log['stats'][key] = [value]

            log_line = self.readline()
        result = {'pre_process': parsed_log}
        return result


parser_description="\
List of available parsers:\n\
\tgeneric: (default) A generic log parser. Will transform each line 'key: value'  and  'key: value unity' into entries inside generated file\n\
\tFlashLogParser: A specific parser for Flash2 log\n\
"

if  __name__ =='__main__':
    print("#", ' '.join(sys.argv))

    DESCRIPTION  = 'Vidjil utility to parse log of some preprocess'
    DESCRIPTION += 'By default, work on log with line in "key: value" string format.'
    DESCRIPTION += 'New class as FlashLogParser can be defined to exploit specific format of your preprocess'

    ### Argument parser (argparse)
    parser = argparse.ArgumentParser(description= DESCRIPTION,
                                     epilog='''Example: python %(prog)s -i out/flash2.log -o preprocess.vidjil --parser FlashLogParser''')

    group_options = parser.add_argument_group() # title='Options and parameters')
    group_options.add_argument('--input',  '-i', type=str, default='preprocess.log',    help='input file (%(default)s)')
    group_options.add_argument('--output', '-o', type=str, default='preprocess.vidjil', help='output file (%(default)s)')
    group_options.add_argument('--parser', '-p', type=str, default='generic',           help='Parser to use; by default use \'%(default)s\'')
    group_options.add_argument('--list', '-l',   action="store_true",                   help='List all available parsers')
    args = parser.parse_args()

    if args.list:
        print( parser_description)
        exit()

    logfile = open(args.input, "r")
    if args.parser == "generic":
        log_parser = LogParser(logfile)
    elif args.parser in globals():
        log_parser = globals()[args.parser](logfile)
    else:
        print( "Warning; Parser to use not recognize; use default parser.")
        log_parser = LogParser(logfile)

    log_parser.export("args from preprocess", args.output)
