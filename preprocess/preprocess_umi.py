import time
import os
import shlex
import subprocess

from preprocess import Preprocess
from logparser import LogParser

# This preprocess allow to made an UMI demultiplexing
# It use Calib/Calib cons software
# default UMI size is 4 at each end.


class PreprocessUMI(Preprocess):

    def __init__(self, args):
        self.description = "UMI preprocess. Allow to made a demultiplexing of R1/R2 files (not merged)."
        self.required_files = 2
        self.builded_files = 2
        self.binary = ["calib", "calib_cons"]
        self.args = args

    def execute(self, files_in: list, file_out: str, args:str):
        start = time.time()
        self.check_execution_start(files_in, args)

        paths = os.path.split(file_out.name)
        path_head = paths[0]
        opt_calib = ""
        opt_calib_cons = ""

        #################################
        ### Build a calib parameters file
        #################################
        cmd = ['%s/calib' % args.binaries, "-f", files_in[0], "-r", files_in[1], "-o", file_out.name]
        cmd += shlex.split( "-e 0 -l 3 -m 7 -t 7 -k 8" )
        if files_in[0].endswith(".gz") and files_in[1].endswith(".gz"):
            cmd += shlex.split( "-g" )
            os.system("gunzip -f -k %s %s" % (files_in[0], files_in[1]) )
        cmd += shlex.split( opt_calib )

        print( "\t\tPhase 1/3 (calib clusterisation index)")
        print( "\t\t# %s" % " ".join(cmd) )
        process_calib = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (stdoutdata_calib, stderrdata_calib) = process_calib.communicate()

        if process_calib.returncode > 0:
            self.exportErrorLogs(stdoutdata_calib, stderrdata_calib, " ".join(cmd), f"{path_head}/../calib_error_", start_time=start)
            raise Exception(f"{self.__class__.__name__}: phase 1 FAILED")

        # self.export_logs(stdoutdata_calib, " ".join(cmd), f"{path_head}/../calib_", start_time=start, parser=LogParser)

        ###################################
        ### Rebuild file from UMI cluster
        ###################################

        ### Build a calib parameters file
        cmd = ['%s/calib_cons' % args.binaries,
         "-c", file_out.name+"cluster",
         "-q", files_in[0].replace(".gz",""), files_in[1].replace(".gz",""),
         "-o", f"{file_out.name}_R1", f"{file_out.name}_R2"
        ]
        cmd += shlex.split( opt_calib_cons )

        print( "\t\tPhase 2/3 (calib clusterisation of fastq files)")
        # print( "\t# %s" % cmd )
        print( "\t\t# %s" % " ".join(cmd) )
        process_calib_cons = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (stdoutdata_calib_cons, stderrdata_calib_cons) = process_calib_cons.communicate()
        if process_calib_cons.returncode > 0:
            self.exportErrorLogs(stdoutdata_calib+stdoutdata_calib_cons, stderrdata_calib+stderrdata_calib_cons, " ".join(cmd), f"{path_head}/../umi_error", start_time=start)
            raise Exception(f"{self.__class__.__name__}: Phase 2 FAILED")

        self.export_logs(stdoutdata_calib + stdoutdata_calib_cons, " ".join(cmd), f"{path_head}/../umi_",
                        start_time=start, parser=LogParser)
        return  {
            "files_out": [f"{file_out.name}_R1.fastq", f"{file_out.name}_R2.fastq"], 
            # "logs": [f"{path_head}/../calib_.vidjil", f"{path_head}/../umi_.vidjil"]
            "logs": [f"{path_head}/../umi_.vidjil"]
        }

    def check_execution_start(self, files_in, args):
        super().check_execution_start(files_in, args)
        for binary in ['%s/calib' % args.binaries, '%s/calib_cons' % args.binaries]:
            if not os.path.exists(binary):
                raise Exception(f"{self.__class__.__name__}: No binary {binary} found.")
        return

