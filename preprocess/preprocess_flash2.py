import pathlib
import time
import os
import shlex
import subprocess
import shutil
import sys

from preprocess import Preprocess
from logparser import FlashLogParser

# This script add preprocessing to merge R1 /R2 files. 
# At least one option should be defined to keep R1 or R2 unmerged reads.
# This script use flash2 software.


class PreprocessFlash2(Preprocess):

    def __init__(self, args):
        self.description = "Flash2 preprocess. Allow to merge reads from R1/R2 files."
        self.required_files = 2
        self.builded_files   = 1
        self.binary          = "flash2"
        self.args = args

    def execute(self, files_in: list, file_out: str, args:str):
        start = time.time()
        self.check_execution_start(files_in, args)

        paths = os.path.split(file_out.name)
        path_head = paths[0]
        path_file = paths[1]
        ## Phase 1, reads merger with Flash2
        cmd_flash = ['%s/flash2' % args.binaries,
         files_in[0], files_in[1],
         "-d", path_head,
         "-o", path_file, # file prefix
         "-t", "1",
         "--compress",
        ]
        cmd_flash += shlex.split( args.options_flash2 )
        print( "\t\t# %s" % " ".join(cmd_flash) )

        p_merger = subprocess.Popen(cmd_flash, stdout=subprocess.PIPE, text=True)

        (stdoutdata_flash, stderrdata_flash) = p_merger.communicate()

        if p_merger.returncode > 0:
            print( stdoutdata_flash)
            print( stderrdata_flash)
            self.exportErrorLogs(stdoutdata_flash, stderrdata_flash, " ".join(cmd_flash), f"{path_head}/../flash2_error_", start_time=start)
            raise Exception(f"Class {self.__class__.__name__}: execute failed")

        try:
            if args.keep:
                shutil.copy(file_out.name+'.extendedFrags.fastq.gz', file_out.name)
            else:
                # Try and move file, copy it if there are rights issues (problem with rights)
                try:
                    shutil.move(file_out.name+'.extendedFrags.fastq.gz', file_out.name)
                except PermissionError:
                    shutil.copyfile(file_out.name+'.extendedFrags.fastq.gz', file_out.name)

            with open(file_out.name, 'ab') as base_file:
                if (args.keep_r1):
                    with open(file_out.name+'.notCombined_1.fastq.gz', 'rb') as f2:
                        shutil.copyfileobj(f2, base_file)
                if (args.keep_r2):
                    with open(file_out.name+'.notCombined_2.fastq.gz', 'rb') as f3:
                        shutil.copyfileobj(f3, base_file)

            if not args.keep:
                os.remove(file_out.name+'.notCombined_1.fastq.gz')
                os.remove(file_out.name+'.notCombined_2.fastq.gz')
                ## Remove the histogram provide by Flash2
                os.remove(file_out.name+'.hist')
                os.remove(file_out.name+'.histogram')
                ## Remove if not moved before
                pathlib.Path.unlink(file_out.name+'.extendedFrags.fastq.gz', missing_ok=True)

        except Exception as e:
            print( stdoutdata_flash)
            print( stderrdata_flash)
            print( e )

            raise Exception(f"Class {self.__class__.__name__}: Error at export  and cleaning step")

        self.export_logs(stdoutdata_flash, " ".join(cmd_flash), f"{path_head}/../flash2_", start_time=start,
                        parser=FlashLogParser)
        return  {
            "files_out": [file_out.name],
            "logs": [f"{path_head}/../flash2_.vidjil"]
        }

    def check_execution_start(self, files_in, args):
        super().check_execution_start(files_in, args)
        if not args.keep_r1 and not args.keep_r2:
            sys.tracebacklimit = 0
            raise Exception(f"{self.__class__.__name__}: Need to define at least one kept file in case of not merged reads.")
        return
