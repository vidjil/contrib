import sys
import shutil
import subprocess
import argparse
import shlex
import os
import time
import tempfile
import json
from datetime import datetime
import io
from collections import defaultdict
import inspect
import csv

# ==========================
# This script make a merging step, followed by vdj filtering made by vidjil-algo
# This allow to store only a shrunk file with only relevant information for vdj identification
# This allow to speed up following analysis and reduce disk space taken by sequences
# ==========================
from preprocess import Preprocess
from preprocess_utils import exist_color, transform_class_name
from preprocess_vdj import PreprocessVDJfilter
from logparser import LogParser
# TODO: Use a glob pattern to load preprocess from other script
# TODO: import all available script with preprocessXXX in name
from preprocess_dimers import PreprocessDimersFilter  # noqa: F401
from preprocess_flash2 import PreprocessFlash2  # noqa: F401
from preprocess_umi import PreprocessUMI  # noqa: F401

PREPROCESS_SCRIPT_VERSION = 0.2


def get_subclasses(cls, include_original=False):
    # Need to be called from this script and not from utils
    return [sub for sub in inspect.getmembers(sys.modules[__name__], inspect.isclass) if issubclass(sub[1], cls) and cls != sub[1]]

subclass = get_subclasses(Preprocess)
print( subclass)
AVAILABLE = {}

for elt in subclass:
    AVAILABLE[transform_class_name(elt[0])] = elt[1]

print(AVAILABLE)

# TODO: instantiate args from preprocess


class PreprocessManager():
    def __init__(self, asked_preprocess: list, tmp_dir: str, files_in:list, file_out:str, args):
        
        self.available = AVAILABLE
        for preprocess in self.available:
            # TODO: only asked one ?
            self.available[preprocess] = self.available[preprocess](args)
        self.ordered_preprocess = ["umi", "flash2", "dimers", "vdj"]
        self.tmp_dir = tmp_dir
        self.files_logs = []
        self.files_in = files_in
        self.file_out = file_out
        self.args = args
        start_number_file = len(files_in)
        end_number_file = 1 # len(file_out); always finish preprocess to 1

        self.ordered_preprocess_to_do = ["raw file(s)"]

        last_preprocess = None
        for preprocess in self.ordered_preprocess:
            if asked_preprocess[preprocess]:
                last_preprocess = preprocess
                self.ordered_preprocess_to_do.append(preprocess)
                # check that all inked preprocess can be followed
                if self.available[preprocess].required_files != start_number_file:
                    raise Exception(f"PreprocessManager: bad size of file in ({start_number_file} vs {self.available[preprocess].required_files}) for {self.available[preprocess].__class__.__name__}")
                else:
                    # replace in files by out files of preprocess
                    start_number_file = self.available[preprocess].builded_files

        if not last_preprocess:
            raise Exception("No preprocess asked")
        if self.available[last_preprocess].builded_files != 1: # always finish preprocess to 1
            raise Exception("PreprocessManager: bad size of file in")

        if args.verbose: 
            print( f"asked_preprocess: {asked_preprocess}")
        print( f"Preprocess workflow: {' -> '.join(self.ordered_preprocess_to_do)}")
        print( f"\tstart_number_file: {len(files_in)}; end_number_file: {end_number_file}")

        # self.execute(asked_preprocess, files_in, file_out, args)
        return

    def description(self):
        print(f"Preprocessing script for vidjil server (ver {PREPROCESS_SCRIPT_VERSION}).\n")
        for preprocess in self.ordered_preprocess:
            print( f"\t{self.available[preprocess].getDescription()}" )
        return

    def execute(self, asked_preprocess:list , files_in: list, file_out: str, args):
        # raise Exception(f"PreprocessManager: Not implemented")
        if not len(asked_preprocess):
            raise Exception("No preprocess asked")

        for preprocess in self.ordered_preprocess:
            if asked_preprocess[preprocess]:
                print(f"\t... Apply preprocess {preprocess}")
                file_tmp = tempfile.NamedTemporaryFile(dir=self.tmp_dir.name, prefix=f"{preprocess}_", delete=False)
                files    = self.available[preprocess].execute(files_in, file_tmp, args)
                self.files_logs += files["logs"]
                self.get_stats_current_file(preprocess, files["files_out"])
                files_in = files["files_out"]

        print(f"\t... Move last preprocess file {files_in[0]} to {file_out}")
        shutil.move(files_in[0], file_out)
        return


    def get_stats_current_file(self, preprocess, files_in):
        start_time=time.time()
        ##############################################
        ### Stats on preprocess; count number of reads
        # Use seqkit toolbox (https://github.com/shenwei356/seqkit)
        cmd_stats = f"{self.args.binaries}/seqkit stats {' '.join(files_in)}"
        print( "\t\t# %s" % cmd_stats )
        process_stats = subprocess.Popen(shlex.split(cmd_stats), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (pstats_stdoutdata, pstats_stderrdata) = process_stats.communicate()
        if process_stats.returncode > 0:
            raise EnvironmentError("\tPhase stats FAILED")

        """ Convert seqkit data into pandas dataframe to export it as formatted data inside log """
        string_csv = pstats_stdoutdata.decode()
        # Format string that is already space align, remove double space, replace simple space by coma
        while "  " in string_csv:
            string_csv = string_csv.replace("  ", " ")
        string_csv = string_csv.replace(",", ".")
        string_csv = string_csv.replace(" ", ",")

        csv_file  = io.StringIO(string_csv)
        reader    = csv.DictReader(csv_file, delimiter=",")
        data_dict = [row for row in reader]

        # Export value as string in log parser compatible format
        log = ""
        cols_to_keep = ["format", "type", "num_seqs", "min_len", "avg_len", "max_len"]
        for index, row in enumerate(data_dict):
            for col in cols_to_keep:
                if col in ["num_seqs", "min_len", "max_len"]:
                    log += f"{col}: {row[col].replace('.', '')}\n"
                else:
                    log += f"{col}: {row[col]}\n"

        if preprocess in self.available:
            self.available[preprocess].export_logs(log, "process_stats", f"{path_head}/{preprocess}_seqkit",
                                                  start_time=start_time, parser=LogParser)
        else:
            # Take a default class as abstracted one can't be selected
            PreprocessVDJfilter(self.args).export_logs(log, "process_stats", f"{path_head}/{preprocess}_seqkit",
                                                      start_time=start_time, parser=LogParser)
        return


    def export_logs(self, asked_preprocess:list , file_out: str, args):
        """
        This function allow to take every parsed logs of each preprocess and aggregate them inside one pre_process.vidjil file
        """
        paths = os.path.split(file_out)
        path_head = paths[0]

        print("\t... Parse and export logs")
        try :
            data = {
                "producer":[f"classed_preprocess (version {PREPROCESS_SCRIPT_VERSION})"], 
                "preprocess_workflow": f"{' -> '.join(self.ordered_preprocess_to_do)}",
                "input":[self.files_in], 
                "output":[file_out], 
                "commandline":[[]], 
                "stats": {},
                "parameters": {},
                "run_timestamp":[datetime.now().strftime('%Y-%m-%d %H:%M:%S')],
                "number": 1,
                "time_taken": {},
                "seqkit": defaultdict(lambda: defaultdict(lambda: {})),
            }
            reads = {"merged": [0], "total": [0], "preprocess_step": {}}

            # Store seqkit data of raw file; allow to get ratio of demultiplexing or prefiltering
            self.get_stats_current_file("raw_files", self.files_in)
            subprocess_seqkit_data = json.load(open(path_head + "/" + "raw_files_seqkit.vidjil"))
            reads["total"] = subprocess_seqkit_data["pre_process"]["stats"]["num_seqs"]
            for stats in subprocess_seqkit_data["pre_process"]["stats"]:
                data["seqkit"][stats]["raw_files"] = subprocess_seqkit_data["pre_process"]["stats"][stats][0]

            if args.verbose:
                print(f"{self.files_logs=}")
            for log_file in  self.files_logs:
                if args.verbose:
                    print( f"{log_file=}")
                preprocess_name = os.path.split(log_file)[1].replace(".vidjil", "")
                subprocess_data = json.load(open(log_file))
                subprocess_seqkit_data = json.load(open(log_file.replace(".vidjil", "seqkit.vidjil")))
                data["commandline"][0].append(subprocess_data["pre_process"]["commandline"][0])

                # convert dict value as array
                if "stats" in subprocess_data["pre_process"]:
                    for key in subprocess_data["pre_process"]["stats"]:
                        data["stats"][f"{preprocess_name}__{key}"] = subprocess_data["pre_process"]["stats"][key]
                if "parameters" in subprocess_data["pre_process"]:
                    for key in subprocess_data["pre_process"]["parameters"]:
                        data["parameters"][f"{preprocess_name}__{key}"] = subprocess_data["pre_process"]["parameters"][key]

                if "time_taken" in subprocess_data["pre_process"]["stats"]:
                    data["time_taken"][preprocess_name] = subprocess_data["pre_process"]["stats"]["time_taken"]
                for stats in subprocess_seqkit_data["pre_process"]["stats"]:
                    data["seqkit"][stats][preprocess_name] = subprocess_seqkit_data["pre_process"]["stats"][stats][0]

                # update reads number 
                reads["merged"] = subprocess_seqkit_data["pre_process"]["stats"]["num_seqs"]


            print(f"\t\tWrite exported logs to {path_head}/pre_process.vidjil")
            with open(f"{path_head}/pre_process.vidjil", "w") as logs_file:
                if args.indent:
                    json.dump({"pre_process": data, "reads": reads}, logs_file, indent=2)
                else:
                    json.dump({"pre_process": data, "reads": reads}, logs_file)

        except IOError :
            os.remove(f_out)
            raise



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Script to apply preprocess given a list of preprocess to do by argument list')

    parser.add_argument("--verbose", "-v", help="Verbose mode", action = 'store_true')

    group_mandatory = parser.add_argument_group('Mandatory arguments')
    group_mandatory.add_argument("--binaries",   help="path to executable")
    group_mandatory.add_argument("--file-r1", "-r1", help="forward read file")
    group_mandatory.add_argument("--file-r2", "-r2", help="reverse read file (if needed by preprocess)")
    group_mandatory.add_argument("--output", "-o",   help="output file")
    group_mandatory.add_argument("--directory", help="output directory")

    group_preprocess = parser.add_argument_group('Preprocess to apply')
    group_preprocess.add_argument("--flash2",    help="Apply flash2 preprocess", action='store_true')
    group_preprocess.add_argument("--umi",       help="Apply UMI preprocess",    action='store_true')
    group_preprocess.add_argument("--vdj", help="Apply V(D)J prefilter",   action='store_true')
    group_preprocess.add_argument("--dimers", help="Apply primer dimers prefilter (remove sequence under 60nt)",   action='store_true')

    group_options = parser.add_argument_group('Preprocess options')
    group_options.add_argument("--keep-r1", help="Merger; keep unmerged forward reads", action="store_true")
    group_options.add_argument("--keep-r2", help="Merger; keep unmerged reverse reads", action="store_true")
    group_options.add_argument("--options-flash2", help="additional options passed to Merger (Flash2)", default="-M 300")
    group_options.add_argument("--options-umi",    help="additional options passed to UMI demultiplexer (Calib)", default="")
    group_options.add_argument("--options-vdj",    help="additional options passed to VDJ prefilter (Vidjil-algo)", default="")
    group_options.add_argument("--options-dimers", help="additional options passed to dimers prefilter (awk command) ", default="")

    group_options = parser.add_argument_group('Script options')
    group_options.add_argument("--description", help="Print description of all available preprocess of this script", action = 'store_true')
    group_options.add_argument("--indent",      help="Print logs in indent json mode", action = 'store_true')
    group_options.add_argument("-k", "--keep",  help="keep temporary files (may take lots of disk space in the end)", action = 'store_true')


    args  = parser.parse_args()
    f_r1  = args.file_r1
    f_r2  = args.file_r2
    f_out = args.output
    opt_flash2 = args.options_flash2
    opt_umi    = args.options_umi
    opt_vdj    = args.options_vdj
    opt_dimers = args.options_dimers

    if not args.directory:
        target_directory = os.path.split(args.output)[0]
    else: 
        target_directory = args.directory

    tmp_dir = tempfile.TemporaryDirectory(dir=target_directory)

    asked_preprocess = {
        "flash2": args.flash2,
        "umi": args.umi,
        "vdj": args.vdj,
        "dimers": args.dimers,
    }

    paths =  os.path.split(f_out)
    path_head = paths[0]
    path_file = paths[1]
    print( "command line: %s" % sys.argv)
    print( "args: %s" % args)
    print( "###" )
    print( "f_r1: %s" % f_r1)
    print( "f_r2: %s" % f_r2)
    print( "f_out: %s" % f_out)
    print( "path_head: %s" % path_head)
    print( "path_file: %s" % path_file)
    print( "target_directory: %s" % target_directory)
    
    print( f"options flash2: {opt_flash2}")
    print( f"options umi: {opt_umi}")
    print( f"options vdj: {opt_vdj}")
    print( f"options dimers: {opt_dimers}")

    # print( "file_merge: %s" % file_merge.name)
    # print( "file_merge_out: %s" % file_merge_out.name)

    print( "\n===== Availability of software")
    # TODO: do that by check of all preprocess loaded
    exit_calib      = os.path.exists('%s/calib' % args.binaries)
    exit_calib_cons = os.path.exists('%s/calib_cons' % args.binaries)
    exit_flash2     = os.path.exists('%s/flash2' % args.binaries)
    exit_seqkit     = os.path.exists('%s/seqkit' % args.binaries)
    print ("calib exists: "      + exist_color(exit_calib))
    print ("calib cons exists: " + exist_color(exit_calib_cons))
    print ("flash2 exists: " + exist_color(exit_flash2))
    print ("seqkit exists: " + exist_color(exit_seqkit))
    print( "=======================\n")

    files_in = [f_r1, f_r2] if f_r2 is not None else [f_r1]

    preprocess_manager = PreprocessManager(asked_preprocess, tmp_dir, files_in, f_out, args)

    if args.description:
        preprocess_manager.description()
        exit()

    preprocess_manager.execute(asked_preprocess, files_in, f_out, args)
    preprocess_manager.export_logs(asked_preprocess, f_out, args)

    if args.keep:
        destination = f"{path_head}/preprocess_tmp"
        print( f"Keep asked; move directory {tmp_dir.name} to {destination}" )
        shutil.move(tmp_dir.name, destination)
