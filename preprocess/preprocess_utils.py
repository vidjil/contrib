import sys
import gzip
import subprocess
import os
import inspect
import importlib.util


def exist_color(exist):
    color = "\033[32m" if exist else "\033[91m"
    return "%s%s\033[0m" % (color, exist)


def is_gzip(file_path):
    try:
        with gzip.open(file_path, 'rb') as f:
            f.read(1)
        return True
    except OSError:
        return False


def get_fastx_format(file_path):
    zipped = is_gzip(file_path)
    try:
        if zipped:
            f = gzip.open(file_path, 'rb')
            first_line = str( f.read(1), 'UTF-8')
        else:
            f = open(file_path, 'r')
            first_line = f.read(1)
        # print( f"first_line: {first_line}")
        if first_line[0] =="@":
            return "fastq"
        if first_line[0] ==">":
            return "fasta"
        return None
    except OSError:
        return None

def count_fasta(file_in):
    # not always real fasta, so count number of '>' instead of line'
    count = 0
    for line in file_in:
        if line[0] == ">":
            count += 1
    return count

def count_fastq(file_in, zipped):
    if zipped:
        zcat_command = ["zcat", file_in.name]
    else:
        zcat_command = ["cat", file_in.name]
    awk_command = ['awk', 'END {print NR/4}']

    with subprocess.Popen(zcat_command, stdout=subprocess.PIPE) as zcat_proc:
        result = subprocess.run(awk_command, stdin=zcat_proc.stdout, stdout=subprocess.PIPE, text=True)
        zcat_proc.stdout.close()

    read_count = result.stdout.strip()

    return read_count


def count_sequence(fi, zipped, fastx_format):
    try:
        if zipped:
            f = gzip.open(fi, 'r')
            if fastx_format == "fasta": 
                return count_fasta(f)
            if fastx_format == "fastq": 
                return count_fastq(f, zipped)
        else:
            f = open(fi, 'r')
            if fastx_format == "fasta": 
                return count_fasta(f)
            if fastx_format == "fastq": 
                return count_fastq(f, zipped)

        return None
    except OSError:
        return None


def get_subclasses(cls, include_original=False):
    # return [(k,v) for k, v in vars(collections.abc).items() if inspect.isclass(v) and issubclass(cls, v) ]
    # return [sub[0] for sub in inspect.getmembers(sys.modules[__name__], inspect.isclass) if issubclass(sub[1], cls) and sub[1] != cls]
    return [sub[0] for sub in inspect.getmembers(sys.modules[__name__], inspect.isclass) if issubclass(sub[1], cls)]


def transform_class_name(name):
    return name.lower().replace("preprocess", "").replace("prefilter", "").replace("filter", "")

def load_sibling_scripts(directory, prefix):
    scripts = []
    for file in os.listdir(directory):
        if file.startswith(prefix) and file.endswith('.py'):
            spec = importlib.util.spec_from_file_location(file[:-3], f"{directory}/{file}")
            script = importlib.util.module_from_spec(spec)
            scripts.append((script, spec))
    return scripts
