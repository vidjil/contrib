import os
import time
from abc import ABC, abstractmethod
from typing import List

from logparser import LogParser

class Preprocess(ABC):

    def __init__(self, args):
        self.description = "Generic class for preprocess. Never call it directly."
        self.required_files = 2
        self.builded_files   = 2
        self.binary          = False
        self.args = args

    def get_description(self):
        print(f"Class {self.__class__.__name__}: {self.description}.\n\tBinary: {self.binary}; Required file(s): {self.required_files}; output file(s): {self.builded_files}")

    
    def get_paths(self, file_path):
        paths =  os.path.split(file_path.name)
        return {"head": paths[0], "file": paths[1], "paths": paths}

    def check_binaries(self):
        """
        Check if binaries exist and have execution rights.
        """
        if self.binary:
            binaries = [self.binary] if isinstance(self.binary, str) else self.binary
            self.check_binaries_list(binaries)
        return
    
    def check_binaries_list(self, binaries: List[str]):
        for binary in binaries:
            if not os.path.exists(self.args.binaries + "/" + binary):
                raise Exception(f"Checking preprocess show not existent binary '{binary}' at path '{self.args.binaries}'")
            if not os.access(self.args.binaries + "/" + binary, os.X_OK):
                raise Exception(f"Checking preprocess show binary '{binary}' is not executable")

    def check_execution_start(self, files_in, args):
        self.check()
        
        if (len(files_in) != self.required_files):
            raise Exception("Checking preprocess show bad number of files given in argument")

        for fi in files_in:
            if not os.path.exists(fi):
                raise Exception(f"File in '{fi}' don't exist")

    def check_execution_end(self, files_out, logs):
        for each in files_out:
            if not os.path.exists(each):
                raise Exception("Checking preprocess finish state (output files, logs)")

    def export_logs(self, stdoutdata, command, path_out, start_time, parser=LogParser):
        if isinstance(stdoutdata, bytes):
            logfile = open(f"{path_out}.log", mode="w", encoding="utf-8")
            logfile.write(str(stdoutdata, 'UTF-8'))
        elif isinstance(stdoutdata, str):
            logfile = open(f"{path_out}.log", mode="w", encoding="utf-8")
            logfile.write(stdoutdata)    
        else:
            raise Exception(f"Unknown type of data for preprocess {self.__class__.__name__}: type {type(stdoutdata)}")

        logfile.write(f"time taken: {time.time() - start_time}")    
        logfile.seek(0)
        logfile.close()

        output_file = f"{path_out}.vidjil"
        print( f"\t\t{self.__class__.__name__}:log output file: {output_file}")


        log_parser  = parser(open(f"{path_out}.log", "r"))
        log_parser.export(command, output_file, one_line=False)
        return

    def export_error_logs(self, stdoutdata, stderrdata, command, path_out, start_time):
        # with tempfile.NamedTemporaryFile(mode="w+") as logfile:
        print("=== stdoutdata ===\n")
        print( stdoutdata )
        print("=== stderrdata ===\n")
        print( stderrdata )

        if isinstance(stdoutdata, bytes):
            logfile = open(f"{path_out}.log", mode="w", encoding="utf-8")
            logfile.write("=== stdoutdata ===\n")
            logfile.write(str(stdoutdata, 'UTF-8'))
            logfile.write("=== stderrdata ===\n")
            logfile.write(str(stderrdata, 'UTF-8'))
        elif isinstance(stdoutdata, str):
            logfile = open(f"{path_out}.log", mode="w", encoding="utf-8")
            logfile.write("=== stdoutdata ===\n")
            logfile.write(stdoutdata)
            logfile.write("=== stderrdata ===\n")
            logfile.write(stderrdata)
        else:
            raise Exception(f"Unknown type of data for preprocess {self.__class__.__name__}: type {type(stdoutdata)}")

        logfile.write(f"time taken: {time.time() - start_time}")    
        logfile.seek(0)
        logfile.close()

        output_file = f"{path_out}.vidjil"
        print( f"\t\t{self.__class__.__name__}:log output file: {output_file}")
        return

    @abstractmethod
    def execute(self):
        pass
        # raise Exception("Default 'EXECUTE' function of generic Preprocess object. DO NOT USE!")
        
    # @abstractmethod
    def check(self):
        """
        """
        # Check seqkit (used for every preprocess)
        self.check_binaries_list(["seqkit"])
        # Check specific binaries
        self.check_binaries()
        return
