import os
import time
import subprocess

from preprocess import Preprocess
from preprocess_utils import is_gzip, get_fastx_format, count_sequence
from logparser import LogParser

# This preprocess allow to remove reads under a given size (by default 60 nt).
# use a simple awk command (already present in major linux distribution)
# Launch on single sequence file (after merge step)


class PreprocessDimersFilter(Preprocess):

    def __init__(self, args):
        self.description = "Prefilter primer dimers. Remove sequence shorter than 60nt (by default) that can be primers dimers"
        self.required_files = 1
        self.builded_files   = 1
        self.binary          = False
        self.args = args

    def execute(self, files_in: list, file_out: str, args:str):
        start = time.time()
        self.check_execution_start(files_in, args)

        paths = os.path.split(file_out.name)
        path_head = paths[0]

        is_zipped = is_gzip(files_in[0])
        fastx_format = get_fastx_format(files_in[0])
        cmd_cat = ["zcat", files_in[0]] if is_zipped else ["cat", files_in[0]]
        
        if fastx_format == "fastq":
            awk_args = " {if (NR % 4 == 1) { header = $0; } else if (NR % 4 == 2) { seq = $0; seq_len = length($0); } else if (NR % 4 == 3) { plus = $0; } else if (NR % 4 == 0) { qual = $0; if(seq_len >= 60) { print header; print seq; print plus; print qual; } } } "
        elif fastx_format == "fasta":
            awk_args = " {if (NR % 2 == 1) { header = $0; } else if (NR % 2 == 0) { seq = $0; seq_len = length($0); if(seq_len >= 60) { print header; print seq; } } }"
        else:
            raise Exception(f"Dimers filter failed; unknown file format; zipped: {is_zipped}; fastx_format: {fastx_format}")
        
        cmd_filter = ["awk", awk_args]
        gzip_command = ['gzip', '-c'] 

        print( "\t\t# %s" % " ".join(cmd_cat) )
        print( "\t\t# %s" % " ".join(cmd_filter) )

        with open(file_out.name, 'w') as output_file:
            with subprocess.Popen(cmd_cat, stdout=subprocess.PIPE) as cat_proc:
                with subprocess.Popen(cmd_filter, stdin=cat_proc.stdout, stdout=subprocess.PIPE, text=True) as awk_proc:
                    cat_proc.stdout.close() 
                    with subprocess.Popen(gzip_command, stdin=awk_proc.stdout, stdout=output_file) as gzip_proc:
                        awk_proc.stdout.close()
                        gzip_proc.wait()

        logs = f"start reads: {count_sequence(files_in[0], is_zipped, fastx_format)}\nend reads: {count_sequence(file_out.name, True, fastx_format)}\n"

        cat_cmd = " | ".join([
            str(" ".join(cmd_cat)), 
            str(" ".join(cmd_filter)), 
            str(" ".join(gzip_command))
        ])
        self.export_logs(logs, cat_cmd, f"{path_head}/../dimers_", start_time=start, parser=LogParser)

        # shutil.move(file_out.name+'.detected.vdj.fa.gz', file_out.name)
        return {"files_out": [file_out.name], "logs": [f"{path_head}/../dimers_.vidjil"]}
