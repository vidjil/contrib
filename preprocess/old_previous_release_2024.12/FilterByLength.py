
import os
import argparse


def process(lines=None):
    ks = ['name', 'sequence', 'optional', 'quality']
    return {k: v for k, v in zip(ks, lines)}

class FastqParser:
    def __init__(self, filePath, fo, length):
        if not os.path.exists(filePath):
            raise SystemError("Error: File does not exist\n")
        if filePath.endswith('.gz'):
            self._file = gzip.open(filePath)
        else:
            self._file = open(filePath, 'rU')
        fastqList = []
        n = 4
        i=0
        mini = 180
        lines = []

        for line in self._file:
            lines.append(line.rstrip())
            if len(lines) == n:
                record = process(lines)
                if len(record["sequence"]) > length:
                    fastqList.append(record)
                i+= 1
                lines = []



        print( "Found %s reads" % len(fastqList))
        print( "i %s" % i)
        self.fastqList = fastqList

        with open(fo, 'w') as fh:
            for fastq in self.fastqList:
                fh.write("\n".join(  [fastq['name'], fastq['sequence'], fastq['optional'], fastq['quality']])+"\n" )


if  __name__ =='__main__':
    DESCRIPTION = 'Vidjil utility to filter fastq file by length of the reads'
    
    #### Argument parser (argparse)

    parser = argparse.ArgumentParser(description= DESCRIPTION,
                                    epilog='''Example:
  python2 %(prog)s --length 60 file.fastq''',
                                    formatter_class=argparse.RawTextHelpFormatter)


    group_options = parser.add_argument_group() # title='Options and parameters')
    group_options.add_argument('--length', "-l", type=int, default=60, help='minimal length to keep reads')
    group_options.add_argument('--input',  '-i', type=str, help='input file')
    group_options.add_argument('--output', '-o', type=str, help='output file')
    args = parser.parse_args()


    print("### FilterByLength.py -- " + DESCRIPTION)
    print("file: %s" % args.input)
    
    fastq = FastqParser(args.input,args.output,args.length)
