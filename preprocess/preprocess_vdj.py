import os
import time
import subprocess
import shutil

from preprocess import Preprocess
from logparser import VidjilLogParser

# This script allow to launch a VDJ prefilter preprocessing step.
# This filter is done by vidjil-algo and keep only read with at least few kmer of V/J genes.
# These gene are defined by a .g germline file. A default one is provided in this repository. 
# A germline directory should be provided and copy to binaries directory. germline file vdj_filter.g should be place in this directory.


class PreprocessVDJfilter(Preprocess):

    def __init__(self, args):
        self.description = "Prefilter VDJ preprocess. Allow to reduce the number of reads to keep by a call to vidjil-algo alpha prefilter on an already merged files"
        self.required_files = 1
        self.builded_files   = 1
        self.binary          = "vidjil-algo-alpha"
        self.args = args
        self.germline_file = f"{self.args.binaries}/germline/vdj_filter.g"

    def check(self):
        super().check()
        if not os.path.exists(self.germline_file):
            raise Exception(f"{self.__class__.__name__}: germline filter file don't exist ({self.germline_file}).")

    def execute(self, files_in: list, file_out: str, args:str):
        start = time.time()
        self.check_execution_start(files_in, args)

        paths =  os.path.split(file_out.name)
        path_head = paths[0]
        path_file = paths[1]

        cmd_filter = ['%s/vidjil-algo-alpha' % args.binaries,
         "-g", self.germline_file,
         "--filter-reads", "--gz",
         "--dir", path_head,
         "--base", path_file,
         files_in[0]
        ]
        print( "\t\t# %s" % " ".join(cmd_filter) )

        p_filter = subprocess.Popen(cmd_filter, stdout=subprocess.PIPE, text=True)

        (stdoutdata_filter, stderrdata_filter) = p_filter.communicate()

        if p_filter.returncode > 0:
            print( stdoutdata_filter)
            print( stderrdata_filter)
            self.exportErrorLogs(stdoutdata_filter, stderrdata_filter, " ".join(cmd_filter), f"{path_head}/../vdj_error_", start_time=start)
            raise Exception("vidjil-algo filter failed")

        self.export_logs(stdoutdata_filter + "\n", " ".join(cmd_filter), f"{path_head}/../vdj_", start_time=start,
                        parser=VidjilLogParser)

        shutil.move(file_out.name+'.detected.vdj.fa.gz', file_out.name)
        return  {"files_out": [file_out.name], "logs": [f"{path_head}/../vdj_.vidjil"]}

    def check_execution_start(self, files_in, args):
        super().check_execution_start(files_in, args)
        if not os.path.exists(self.germline_file):
            raise Exception( f"{self.__class__.__name__}: Germline filter file '{self.germline_file}' don't exist" )
        return

