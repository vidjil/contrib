#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This file is a demonstration on a way to create sets, upload samples in them and launch analysis on a server.
It is based on work done at La Pitié Salpetrière, Paris and use in their regular worklfow.

You need to get api_vidjil.py file corresponding for your server:
* Server based on web2py (<2024) at https://gitlab.inria.fr/vidjil/vidjil/-/blob/d40562de3ff572747f0bafe3ff940a1b2f61e215/tools/api_vidjil.py for the lastest stable version.
* Server based on py4web (since 2024) at https://gitlab.inria.fr/vidjil/vidjil/-/blob/dev/tools/api_vidjil.py for the lastest version.

It use a CSV file with some defined fields (first_name,last_name,birth,comments,file_r1,file_r2,sampling_date,info,generic_set)
Each line is a sample that should be associated with:
* a patient, create each time, even if already present
* a run, created if not present
* an optionnal generic set, also created if not already present
After upload of sample, an analysis is automatically launch with config IGH (take care that config id may differ on each server).

## User and password can be hardcoded in file to be use for login (see variables LOCAL_USER and LOCAL_PASSWORD).
## If not define here, a prompt will appear at launch to fill these field
## The best security mix is to store your email login and to fill password with the integrated prompt at launch.

## !!! BE AWARE TO NOT SHARE A SCRIPT WITH PASSWORD FOR SECURITY REASONS !!!
"""


from api_vidjil import Vidjil, is_valid_date
import argparse
import os
import getpass
import csv


### Target server (see doc/server.md)
## See documentation to find how to get ssl certificate
PUBLIC_SERVER = "https://localhost/vidjil/"
PUBLIC_SSL = "localhost-chain.pem"
# You can set here user login and password.
# If not, value will be asked at launch of this script

# Keep in mind that password will be here not hash and readable by everyone. So please don't do it for production server target
# PUBLIC_USER = "your user mail"
# PUBLIC_PASSWORD = "your password"


## Parser, allow to get parameters to execute this scripts
parser = argparse.ArgumentParser(description= 'Vidjil API workflow of La Pitié Salpetrière')
parser.add_argument('--csv',      help='CSV file with informations (see pitie.csv)', required=True)
parser.add_argument('--run_name', help='Name of the run to use/create. It is not defined in CSV file', required=True)
parser.add_argument('--run_date', help='Date of launch ot the run (format YYYY-MM-DD)', required=True)


def is_same_patient(patient_info, patient_getted):
    """Return True if a patient have same identity information (names and birth)
    Make a lower transformation of string to compare them, but don't do more (-/space between name for example)

    Args:
        patient_info (dict): A dict obtain from csv with a least first_name, last_name and birth fields
        patient_getted (dict): A dict obtain from patient resquest (should contains a least first_name, last_name and birth fields)

    Returns:
        Boolean: True if an exact match is found
    """
    if patient_info['first_name'].lower() == patient_getted['first_name'].lower() and \
        patient_info['last_name'].lower() == patient_getted['last_name'].lower() and \
        patient_info['birth'] == patient_getted['birth']:
            # print(f"is_same_patient: \n\tlast {patient_info['last_name']} vs {patient_getted['last_name']} \n\tlast {patient_info['first_name']} vs {patient_getted['first_name']} \n\tlast {patient_info['birth']} vs {patient_getted['birth']} ")
            return True
    else:
        return False


def demoWriteRunOnServer(server, ssl, user, password):
    """
    This demo requires a server/login with write access.
    It creates patients/set/runs, upload data, and run analysis.
    Do not spam a production server!
    """

    # Login
    vidjil = Vidjil(server, ssl=ssl)
    vidjil.login(user, password)

    vidjil.setGroup() # Fill here value of your group. 
    # A list of your available groups can be found in the log of this script after vidjil.login() on recent server version. 
    # On older, you should send us a mail to get it.

    # Look if a run with same name already exist to use it if available; else create it.
    # Take care that this function will made a filter on your avaialble runs.
    # if run name is a subset of existing one, it will be found either.
    runs = vidjil.getSets(set_type=vidjil.RUN, filter_val=args.run_name)
    if not is_valid_date(args.run_date):
        raise Exception( f"Run date given is not correct format or value (YYYY-MM-DD) -> {args.run_date}")

    print( f"Looking for runs with name: {args.run_name}")
    if len(runs["query"]) > 1:
        Exception(f"More than one run with same name exists: {len(runs)}")
        print( runs )
        vidjil.infoSets("Runs", runs["query"], vidjil.RUN, verbose=True)
        raise Exception
    elif len(runs["query"]) == 1:
        run_data = runs["query"][0]
        print( f"Run already exists; use it. Run found: {run_data}")
        setid_run =  run_data['sample_set_id']
    elif len(runs["query"]) == 0:
        print( f"No previous run with same name; Creation if a new one: {args.run_name}")
        run_data = vidjil.createRun(args.run_name, run_date=args.run_date)
        setid_run = run_data["args"]["id"]
        
    print( "==> set run: %s" % setid_run)

    
    
    with open(args.csv) as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=';')
        rows = []
        for row in spamreader:
            rows.append(row)

        # Precheck informations before creating patient, set or samples
        for row in rows:
            # print( f"{row['birth']}; is_valid_date(row['birth'])")
            print( f"{row['sampling_date']}; {is_valid_date(row['sampling_date'])}")
            if row["birth"] != "" and not is_valid_date(row["birth"]):
                raise Exception( f"Birthdate given is not in a correct format or value (YYYY-MM-DD) -> {row['birth']}.\nError occured during precheck, so no patients or samples have been created so far.")
            elif row["sampling_date"] != "" and not is_valid_date(row["sampling_date"]):
                raise Exception( f"Sampling date given is not in a correct format or value (YYYY-MM-DD) -> {row['sampling_date']}.\nError occured during precheck, so no patients or samples have been created so far.")
            elif row["file_r1"] != "" and not os.path.exists(row["file_r1"]): # in this demo, file R1 sghould at least be always present
                raise Exception( f"File R1 don't exist. Please check that provide correct path: {row['file_r1']}.\nError occured during precheck, so no patients or samples have been created so far.")
            elif row["file_r2"] != "" and not os.path.exists(row["file_r2"]):
                raise Exception( f"File R2 don't exist. Please check that provide correct path: {row['file_r2']}.\nError occured during precheck, so no patients or samples have been created so far.")

        for row in rows:
            print( row )
            # Search is a patient with last name, first name and same birthdate exist.
            # Search is based on string and are not case sensitive (MARIE will mathc Marie)
            # WARNING: Keep in mind that "Marie Helene" will not match "Marie-Helene". Use the same name rule taht usual in your laboratory
            # WARNING: Need at least version 2024.11 of server
            patients = vidjil.getSets(set_type=vidjil.PATIENT, filter_val=f"{row['last_name']} {row['first_name']} {row['birth']}")

            if len(patients["query"]):
                found = [patient_data for patient_data in patients["query"] if is_same_patient(row, patient_data)]
                if len(found) == 0:
                    # No exact match, so need to create a new patient
                    patient_data = vidjil.createPatient(first_name=row["first_name"], last_name=row["last_name"], birth_date=row["birth"], info=row["comments"])["args"]
                elif len(found) == 1:
                    print(f"Patient found with exact match (last_name: {row['last_name']}; first_name:{row['first_name']}; birth:{row['birth']})")
                    patient_data = found[0]
                    patient_data["id"] = patient_data["sample_set_id"] # ID of created pateitn correspond to sample_set_id on request result
                else:
                    # At least 2 patients with same information, raise an error
                    raise Exception(f"{len(found)} patients found with same information (last_name: {row['last_name']}; first_name: {row['first_name']}; birth: {row['birth']})\nSimilar patients ids: {[elt['sample_set_id'] for elt in found]}")

            else:
                patient_data = vidjil.createPatient(first_name=row["first_name"], last_name=row["last_name"], birth_date=row["birth"], info=row["comments"])["args"]

            setid_patient = patient_data["id"]
            print( "==> Set patient: %s" % setid_patient)


            # Look if a generic set with same name already exist to use it if available; else create it.
            if row["generic_set"] != "":
                sets_other = vidjil.getSets(set_type=vidjil.SET, filter_val=row["generic_set"])
                if len(sets_other["query"]) > 1:
                    Exception(f"More than one generic sert with same name exists: {len(sets_other)}")
                    vidjil.infoSets("Sets", sets_other["query"], vidjil.SET, verbose=True)
                    raise Exception
                elif len(sets_other["query"]) == 1:
                    set_other = sets_other["query"][0]
                    print( f"Generic set already exists; use it. Generic set found: {set_other}")
                    set_other_id = set_other['sample_set_id']
                    # print(set_other)
                elif len(sets_other["query"]) == 0:
                    print( f"Create new generic set: {row['generic_set']}")
                    set_data = vidjil.createSet(row["generic_set"], info="")
                    set_other_id = set_data["args"]["id"]

                print( "==> set other: %s" % set_other_id)

            # set_ids filed take value in a specific format: ':$set+($id)'
            # Multiple field should be separated with a '|' as above
            # With :
            #   $set can be 's' (generic set), 'p' (patient), or 'r' (run)
            #   $id is the id of the main set 
            setid_generic = f':s+({set_other_id})|' if row["generic_set"] != "" else "" # compose id string depending of generic set present or not
            sample = vidjil.createSample(source="computer",
                        pre_process= "4",
                        set_ids= f"{setid_generic}:r+({setid_run})|:p+({setid_patient})",
                        file_filename= row["file_r1"],
                        file_filename2= row["file_r2"],
                        file_id= "",
                        file_sampling_date= row["sampling_date"],
                        file_info= row["info"],
                        file_set_ids= "",
                        sample_set_id= setid_run,
                        sample_type= "run")

            file_id  = sample["file_ids"][0]  ## Uploaded file
            print( "==> new file %s" % file_id)


            ### Launch analysis on this new sample
            config_id = 2 ## IGH sur app; TRG = 30
            analysis  = vidjil.launchAnalysisOnSample(setid_run, file_id, config_id)
            print("Launch analysis: %s" % analysis)


if  __name__ =='__main__':
    """Examples using Vidjil API"""

    args = parser.parse_args()

    if PUBLIC_USER == "":
        PUBLIC_USER = input("Enter public login user:")
    if PUBLIC_PASSWORD == "":
        print( "Attempted to log as '%s' on public server" % PUBLIC_USER)
        PUBLIC_PASSWORD = getpass.getpass("Password for public server:")


    demoWriteRunOnServer(PUBLIC_SERVER, PUBLIC_SSL, PUBLIC_USER, PUBLIC_PASSWORD)