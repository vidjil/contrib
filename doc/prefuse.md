# Preprocess



``` mermaid
graph LR
  A[vidjil files] --> |prefuse| B[augmented vidjil files];
  A --> C[fused file];
  B --> C;
```


## Mandatory arguments

These arguments are deliver by server with args key. Order is not important as long as it can be interpreted by script.

* **input**: Vidjil input file (-i/--input)
* **output**: Vidjil output file with longer reads for RNAseq (-o/--output)
* **directory**: Vidjil output directory (-d/--directory)

Script need to implement an arguments parsing with these values at least.
See exemple in samples directory.