

compile:
	make -C third-party-softwares/calib
	make -C third-party-softwares/minia
	make -C third-party-softwares/flash2

tests-preprocess:
	make -C preprocess/tests
tests-pre-fuse:
	make -C pre-fuse/tests
tests-post-fuse:
	make -C post-fuse/tests

tests: tests-preprocess tests-pre-fuse tests-post-fuse

all: compile

.PHONY: all compile
