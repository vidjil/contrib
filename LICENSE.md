
# Vidjil: open-source and licenses

Vidjil is open-source, released under GNU GPLv3+ license.
You are welcome to redistribute it under [certain conditions](http://git.vidjil.org/blob/master/doc/LICENSE).
This software is for research use only and comes with no warranty.

The development code is available on <http://gitlab.vidjil.org/>.
Bug reports, issues and patches are welcome.

This dedicated repository is used to share some scripts that be inserted 
as preprocess on raw sequencing files, 
or a pre/post fuse step (merging of multiples analysis) created for and by the community.



# Licenses for third-party software and databases

To work, some scripts need the use of third-party software and libraries under open-source licenses.
Here is a list of software present in scripts.

## Softwares

**Calib/Calib_cons**: Demultiplexing tool for UMI dataset (preprocess)

* @version          v0.3.4
* @author           Baraa Orabi
* @link             <https://github.com/vpc-ccg/calib>
* @license          MIT

**Flash2**          : FLASH (Fast Length Adjustment of SHort reads) is an accurate and fast tool
to merge paired-end reads

* @version          v2.2.0
* @author           Tanja Magoč and Steven L.
* @link             <https://github.com/dstreett/FLASH2>
* @license          GPL-3.0 license

**Minia**           : De novo assembling tools; Used to create contigs 

* @version          v3.2.6
* @author           
* @link             <https://github.com/GATB/minia>
* @license          AGPL-3.0 license 

**Pandas**          : powerful Python data analysis toolkit

* @version          v2.0.3
* @author           Great community of thoushand of people.
* @link             <https://github.com/shenwei356/seqkit>
* @license          BSD-3-Clause license 

**Seqkit**          : a cross-platform and ultrafast toolkit for FASTA/Q file manipulation

* @version          v2.4.0
* @author           Wei Shen
* @link             <https://github.com/shenwei356/seqkit>
* @license          MIT


## vidjil-server

### Software and libraries included in Vidjil repository

To compile these software, usage of a vidjil-server docker image is needed.
To find a complete list of built-in third party software inside vidjil-server image, see the `LICENSE.md` file of the vidjil repository.
