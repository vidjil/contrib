#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" ############################################################
The aim of this script is to get information on coverage of a sequencing. 
It is a vidjil post fuse script. 
A bowtie2 analysis is done against a segments DB, splitted by type of segment (V,D,J).
Each read is analyze againt these Db and a table of match is computed and exported in results directory.

Use to know if sequencing with a set of primer have correct coverage of panel of segments.

Dev: 
    Florian.thonier@inria.fr
############################################################ """


############################################################
### imports
import json
import os
import random, string
import argparse
import sys
import tempfile
from collections import defaultdict
from os.path import isfile, join
import shutil
import gzip
import subprocess
import shlex
import time
from datetime import datetime
from abc import ABC, abstractmethod
import collections.abc
import io
# import pandas as pd
import inspect
import datetime

SCRIPT_VERSION = "v0.1 (beta)"


def exist_color(exist):
    color = "\033[32m" if exist == True else "\033[91m"
    return "%s%s\033[0m" % (color, exist)


def is_gzip(file_path):
    try:
        with gzip.open(file_path, 'rb') as f:
            f.read(1)
        return True
    except OSError:
        return False


def getFastxFormat(file_path):
    """Determine if sequence file is fasta or fastq

    Args:
        file_path (str): _description_

    Returns:
        str: Format value between fasta and fastq
    """
    zipped = is_gzip(file_path)
    try:
        if zipped:
            f = gzip.open(file_path, 'rb')
            first_line = str( f.read(1), 'UTF-8')
        else:
            f = open(file_path, 'r')
            first_line = f.read(1)
        # print( f"first_line: {first_line}")
        if first_line[0] =="@":
            return "fastq"
        if first_line[0] ==">":
            return "fasta"
        return None
    except OSError:
        return None

def countFasta(filein):
    """Cound number of reads inside a fasta file, not zipped

    Args:
        filein (str): file path

    Returns:
        int: Number of reads
    """
    # not always real fasta, so count number of '>' instead of line'
    count = 0
    for line in filein:
        if line[0] == ">":
            count += 1
    return count

def countFastq(filein, zipped):
    """Cound number of reads inside a fasta file, not zipped

    Args:
        filein (str): file path
        zipped (bool): Is sequence file zipped or not

    Returns:
        int: Number of reads
    """
    if zipped:
        zcat_command = ["zcat", filein.name]
    else:
        zcat_command = ["cat", filein.name]
    awk_command = ['awk', 'END {print NR/4}']

    with subprocess.Popen(zcat_command, stdout=subprocess.PIPE) as zcat_proc:
        result = subprocess.run(awk_command, stdin=zcat_proc.stdout, stdout=subprocess.PIPE, text=True)
        zcat_proc.stdout.close()

    read_count = result.stdout.strip()

    # print(f"Nombre de reads : {read_count}")
    return read_count


def countSequence(fi, zipped, fastx_format):
    """Cound number of reads inside a gibven sqequence file, with known zipped and format value

    Args:
        filein (str): file path
        zipped (bool): Is sequence file zipped or not
        fastx_format (str): sequence file format (fasta, fastq)

    Returns:
        int: Number of reads
    """
    try:
        if zipped:
            f = gzip.open(fi, 'r')
            if fastx_format == "fasta": 
                return countFasta(f)
            if fastx_format == "fastq": 
                return countFastq(f, zipped)
        else:
            f = open(fi, 'r')
            if fastx_format == "fasta": 
                return countFasta(f)
            if fastx_format == "fastq": 
                return countFastq(f, zipped)

        return None
    except OSError:
        return None


class Fasta:
    """Allow to manipulate fasta. 
    Will store, by header name, all fasta read of a file
    We don't use bipython to limit size of docker image
    """
    def __init__(self, finame):
        # print("\tCreate class Fasta: ... %s" % finame)
        self.seq = {}
        fi = open( finame, 'r')
        header, sequence = '', ""
        for line in fi:
            if line[0] == '>':
                if sequence: 
                    self.seq[header] = sequence
                if "|" in line:
                    header = line[1:-1].split("|")[1]
                else:
                    header = line[1:-1]
                sequence = ""
            else:
                sequence += line.strip()
        self.seq[header] = sequence
        print( "\t... %s sequences" % len(self.seq))


class Eval():
    """Evaluation object. 
    Will take a vidjil file, extract list of raw sequence file, and launch bowtie analysis
    After that, make an export as csv of evaluation
    """

    def __init__(self, data:dict, path:str, db_bowtie:str, germlines:str, directory:str, output:str, allele:bool = False):
        self.raw_data = data
        self.db_bowtie = db_bowtie
        self.germ_files  = germlines
        self.output = output
        self.directory = directory.name
        self.binaries = "../../binaries/"
        self.allele = allele
        self.segments = ["V", "D", "J", "INTRON"]
        self.path = path
        self.files  = self.extractFilesList()
        self.import_germlines()
        self.computeEval(self.files)
        self.mergeEval()
        self.exportvidjil()
        self.exportCSV()
        self.exportTest()
        return
        
    def extractFilesList(self):
        """ Get list of all original files from vidjil content
        """
        files = self.raw_data["samples"]["original_names"]
        # TODO: check if all sequence files always exist
        return files


    def computeBowtie(self, filein, segment):
        """ Launch a bowtie call on a sequence file
        Bowtie exec is already present in default docker image of vidjil server
        """
        paths =  os.path.split(filein)
        path_head = paths[0]
        path_file = paths[1]
        seq_format = getFastxFormat(filein)

        cmd_bowtie = [f"bowtie2",
         "-x", f"{self.db_bowtie}/germline_{segment}",
         "-U", f"{filein}", f"{'-f' if seq_format == 'fasta' else ''}",
         "-S", f"{self.directory}/eval_{segment}_{path_file}.sam",
         "--local", "-k", "1", "--threads", "1", 
         "--very-sensitive-local", 
         "--minins", f"{120 if segment == 'V' else '30'}"
        ]

        # cmd_bowtie += shlex.split( f_opt )
        print( "\t\t# %s" % " ".join(cmd_bowtie) )

        p_bowtie = subprocess.Popen(cmd_bowtie, stdout=subprocess.PIPE, text=True)
        (stdoutdata_bowtie, stderrdata_bowtie) = p_bowtie.communicate()

        if p_bowtie.returncode > 0:
            print( f"{stdoutdata_bowtie=}")
            print( f"{stderrdata_bowtie=}")
            # self.exportErrorLogs(stdoutdata_bowtie, stderrdata_bowtie, " ".join(cmd_bowtie), f"{path_head}/../bowtie_error_", start_time=start)
            raise Exception(f"Class {self.__class__.__name__}: execute bowtie failed {filein}; {segment}")
        
        return

    def computeSamFile(self, samfile):
        """ For a samfile, list all gene found and give some information on it
        """
        return

    def computeEval(self, files):
        """ Compute for each files the presence or not of germline genes
        """
        for filein in self.files:
            paths =  os.path.split(filein)
            path_head = paths[0]
            path_file = paths[1]

            for segment in self.segments:
                self.computeBowtie(filein, segment)
                self.evaluation(path_file, segment)

        return

    def import_germlines(self):
        """ read germline fasta file, create Fasta obj, put all gene inside a dict """
        print( "... import_germlines")
        # lister tous les fichiers fasta du dossier
        # importer les données
        self.germlines = {}
        # germs: [segment][gene][file]: int
        self.germs_of_files = defaultdict(lambda:defaultdict(lambda:defaultdict(lambda:0)))
        self.germs_raw = defaultdict(lambda:[])
        for segment in self.segments:
            print( f"\timport germ: {segment}")
            germfile_segment = f"{self.db_bowtie}/all_{segment}.fa"
            print( f"germline: {segment}; {germfile_segment}")
            germ = Fasta(germfile_segment)
        
            for seg in germ.seq.keys():
                if not self.cleanAlleleName(seg) in self.germs_raw[segment]:
                    self.germs_raw[segment].append(self.cleanAlleleName(seg))

        return


    def evaluation(self, filein, segment):
        print( "... evaluation")
        

        fi = open(f"{self.directory}/eval_{segment}_{filein}.sam", "r")
        # TODO: Keep only best match ? I have the sensation that we can have multiple hits.

        previous = None
        for line in fi:
            if "LN:" in line:
                pass
            elif line.startswith("@"):
                pass
            else:
                gene = line.split()[2]
                new = line.split()[0]
                if previous != new and gene != "*":
                    self.germs_of_files[segment][self.cleanAlleleName(gene)][filein] += 1
                elif gene != "*":
                    self.germs_of_files[segment]["no match"][path_file] += 1
                previous = new
        return

    def mergeEval(self):
        fo = open("fo_germs_of_files.json", "w")
        json.dump(self.germs_of_files, fo, indent=2)
        return


    def exportvidjil(self, ):
        """ Build a vidjil file with one clonotype by segment ? 
        """
        try:
            self.raw_data["samples"]["producer"] = [f"Panel evaluation (version {SCRIPT_VERSION})"]
                    # "preprocess_workflow": f"{' -> '.join(self.ordered_preprocess_to_do)}",
            self.raw_data["samples"]["commandline"] = []
            self.raw_data["samples"]["run_timestamp"] = [datetime.now().strftime('%Y-%m-%d %H:%M:%S')]
                    
            # reads = {"merged": [0], "total": [0], "preprocess_step": {}}
            self.raw_data["clones"] = []

            for segment in self.segments:
                for gene in self.germs_raw[segment]:
                    seg = "5" if segment == "V" else "3" if segment == "J" else "4" if segment == "D" else "5" if segment == "INTRON" else "5"
                    clone = {
                        "germline": gene[:3],
                        "id": gene,
                        "name": f"{gene[:3]} {gene[3:]}",
                        "reads": [1] if sum([int(value) for value in content_files]) else [0] +\
                          [self.germs_of_files[segment][gene][os.path.split(file_path)[1]] for file_path  in self.files],
                        "seg": {
                            seg: {"name": segment}
                        },
                        "top": 1
                    
                    }

                    self.raw_data["clones"].append(clone)

            fo = open(self.output, "w")
            json.dump(self.raw_data, fo, indent=2)
        except:
            pass
        return


    def exportCSV(self):
        """ 
        Generate one CSV by segment type, with one line by genes. 
        Columns will be first gene name, sum and one more columns by single file
        """
        for segment in self.segments:
            fo_path = f"{self.path}/segment_{segment}.csv"
            print( f"export csv: {fo_path}")
            fo = open(fo_path, "w")
            content_files = ["locus", "gene", "found", "number of positive files"] + [os.path.split(file_path)[1] for file_path  in self.files]
            fo.write(",".join(content_files) + "\n")

            for gene in self.germs_raw[segment]:
                content_files = [str(self.germs_of_files[segment][gene][os.path.split(file_path)[1]]) for file_path  in self.files]
                content_found = ["1"] if sum([int(value) for value in content_files]) else ["0"]
                content_sum   = [str(sum([1 for value in content_files if int(value)]))] 
                content = [gene[:3], gene] + content_found + content_sum + content_files
                # print( content )
                fo.write(",".join(content) + "\n")

        return



    def exportTest(self, ):
        """
        """
        fo = open(self.output, "w")
        json.dump(self.raw_data, fo, indent=2)
        return



    def cleanAlleleName(self, name):
        """ Remove allele from name of gene if setted self.allele to False """
        return name if self.allele else name.split("*")[0]



if  __name__ =='__main__':

    print("#", ' '.join(sys.argv))

    DESCRIPTION = 'Vidjil utility to evaluate primer found in a set a vidjil files'
    print(f"(eval); {datetime.datetime.now().strftime('%Y-%m-%d %H:%M')} # {' '.join(sys.argv)}\n")
    
    # configuration 
    # panel eval
    # vidjil: -c clones -z 1 -r 1 -g germline/homo-sapiens.g -e 1 --no-airr -X 10
    # fuse: -t 100 --post "evaluation_vdj.py --germlines db_panel_vdj"  
    # internal path: /usr/share/vidjil/tools/scripts/post-fuse/


    #### Argument parser (argparse)

    parser = argparse.ArgumentParser(description= DESCRIPTION,
                                    epilog='''Example:
  python3 %(prog)s --germlines germlines --output output --input file.vidjil''',
                                    formatter_class=argparse.RawTextHelpFormatter)

    

    group_mandatory = parser.add_argument_group('Mandatory arguments')
    group_mandatory.add_argument('-i', '--input',  help='Vidjil input file')
    group_mandatory.add_argument('-o', '--output', help='Vidjil output file with normalization')
    group_mandatory.add_argument("--directory", help="output directory")
    group_mandatory.add_argument("--binaries", help="binaries directory", default="/binaries/")
    # group_mandatory.add_argument('-p', '--path',   help='Working directory where analysis will be stored', required=True)


    group_options = parser.add_argument_group('Preprocess options')
    group_options.add_argument('--verbose', action='store_true', default=False, help='run script in verbose mode')
    group_options.add_argument('--germlines', '-g', type=str, default="germline", help='path to germline semgent files')
    group_options.add_argument('--db-bowtie', type=str, default="/binaries/db_panel_vdj", help='path to germline bowtie database (default to ./db_panel_vdj)')
    group_options.add_argument('--allele', '-a', action="store_true", default=False, help='Split by allele of not (not by default)')
    group_options.add_argument("-k", "--keep",  help="keep temporary files (may take lots of disk space in the end)", action = 'store_true')

    args = parser.parse_args()

    inf  = args.input
    outfile = args.output
    msgs = args.verbose
    germlines   = args.germlines
    db_bowtie   = args.db_bowtie
    allele = args.allele
    paths =  os.path.split(outfile)
    path_head = paths[0]
    path_file = paths[1]

    path_in =  os.path.split(inf)
    path_in_head = path_in[0]
    path_in_file = path_in[1]

    binaries  = args.binaries
    print ( "verbose: %s" % msgs)
    print ( "binaries: %s" % binaries)
    print( f"{path_in_head=}")
    print(f"{path_in_file=}" )
    # for key in args:
    #     print(f"\targs {key}: {args['key']}")

    if not args.directory:
        target_directory = os.path.split(args.output)[0]
    else: 
        target_directory = args.directory

    tmp_dir = tempfile.TemporaryDirectory(dir=target_directory)
    # read input file
    if msgs:
        print('Reading input file', file=sys.stderr)

    # Load alreaday fused files
    # Import it as a json dict, and use it for evaluation. Mainly to get filelist
    with open(inf) as inp:
        data = json.load(inp)


    new_data = Eval(data=data, path=path_in_head, db_bowtie=db_bowtie, germlines=germlines, directory=tmp_dir, output=outfile, allele=allele)

    

    if args.keep:
        destination = f"{path_head}/post_process_tmp"
        
        # FOR DEV ONLY
        destination = f"./post_process_tmp"


        print( f"Keep asked; move directory {tmp_dir.name} to {destination}" )
        shutil.move(tmp_dir.name, destination)